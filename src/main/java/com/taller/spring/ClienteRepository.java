package com.taller.spring;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {

	Cliente findByRuc(String ruc);

}
