package com.taller.spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/clientes/{clienteId}/facturas")
public class FacturaCtrl {

	@Autowired
	private FacturaRepository fr;

	@RequestMapping(method = RequestMethod.GET) // api/clientes/234/facturas
	public List<Factura> get(@PathVariable("clienteId") Long clienteId) {
		List<Factura> factureas = fr.findByClienteId(clienteId);
		return factureas;
	}

	@RequestMapping(method = RequestMethod.POST) // api/clientes/234/facturas
	public Factura save(@PathVariable("clienteId") Long clienteId, @RequestBody Factura factura) {

		fr.save(factura);

		return factura;
	}

}
