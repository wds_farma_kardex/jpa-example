package com.taller.spring;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Factura {

	@Id
	@GeneratedValue
	private Long id;

	private Date fecha;

	private Long serial;

	@ManyToOne
	private Cliente cliente;

	@OneToMany
	private List<LineaDetalle> lineas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Long getSerial() {
		return serial;
	}

	public void setSerial(Long serial) {
		this.serial = serial;
	}

	public List<LineaDetalle> getLineas() {
		return lineas;
	}

	public void setLineas(List<LineaDetalle> lineas) {
		this.lineas = lineas;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}
