package com.taller.spring;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FacturaRepository extends JpaRepository<Factura, Long> {

	List<Factura> findByClienteId(Long clienteId);

	Page<Factura> findByClienteId(Long clienteId, Pageable pageable);

}
