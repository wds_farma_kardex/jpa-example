package com.taller.spring;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MK_PRODUCTOS")
public class Producto {

	@Id
	@GeneratedValue
	private Long id; // shitt+alt + s +r

	@Column(unique = true, length = 10)
	private String codigo;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	private String nombre;

	private String descripcion;

	private Double precio;

	private int inventarioInicial;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public int getInventarioInicial() {
		return inventarioInicial;
	}

	public void setInventarioInicial(int inventarioInicial) {
		this.inventarioInicial = inventarioInicial;
	}

}
