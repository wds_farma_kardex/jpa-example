package com.taller.spring;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LineaDetalleRepository extends JpaRepository<LineaDetalle, Long> {

}
