package com.taller.spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/clientes")
public class ClienteCtrl {

	@Autowired
	private ClienteRepository cr;

	@RequestMapping(method = RequestMethod.GET)
	public Page<Cliente> get(Pageable p) {
		Page<Cliente> clientes = cr.findAll(p);
		return clientes;
	}

	@RequestMapping(method = RequestMethod.POST)
	public Cliente save(@RequestBody Cliente c) {
		cr.save(c);
		return c;
	}

}
