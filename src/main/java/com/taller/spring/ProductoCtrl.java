package com.taller.spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/productos")
public class ProductoCtrl {

	@Autowired
	private ProductoRepository pr;

	@RequestMapping(method = RequestMethod.GET)
	public Page<Producto> get(Pageable pageable) {
		Page<Producto> producotssss = pr.findAll(pageable);
		return producotssss;
	}

	@RequestMapping(method = RequestMethod.POST)
	public Producto get(@RequestBody Producto p) {
		pr.save(p);
		return p;
	}

	@RequestMapping(value = "/{idProducto}", method = RequestMethod.PUT) // api/productos/544
	public Producto update(@PathVariable("idProducto") Long id, @RequestBody Producto dataNueva) {
		Producto productoOld = pr.findOne(id);
		productoOld.setNombre(dataNueva.getNombre());
		productoOld.setDescripcion(dataNueva.getDescripcion());
		pr.save(productoOld);
		return productoOld;
	}

	@RequestMapping(value = "/{idProducto}", method = RequestMethod.DELETE)
	public void delete(@PathVariable("idProducto") Long id) {
		pr.delete(id);
	}

}
